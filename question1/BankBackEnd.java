package com.godigit.question1;

import java.util.Scanner;

public class BankBackEnd {
	Scanner scan = new Scanner(System.in);
	private String name=null;
	private String accNo=null;
	private int balance;
	
	public void createAcc() {
			System.out.println("ENTER YOUR NAME");
			name= scan.next();
			System.out.println("ENTER YOUR ACCOUNT NUMBER");
			accNo= scan.next();
			System.out.println("ENTER INITIAL BALANCE");
			balance= scan.nextInt();
			}
	public void deposit(int depositAmount) {
		
		if(depositAmount>0)
		{balance=balance+depositAmount;
		 System.out.println("DEPOSIT DONE");}
		else {System.out.println("INVALID DEPOSIT AMOUNT");}
	}
	public void withdraw(int withdrawAmount) {
		if(withdrawAmount>0)
		{balance=balance-withdrawAmount;
		System.out.println("WITHDRAWAL DONE");}
		else {System.out.println("INVALID WITHDRAWAL AMOUNT");}
	}
	public void displayDetails() {
		System.out.println("NAME:"+name);
		System.out.println("ACCOUNT NUMBER:"+accNo);
		System.out.println("BALANCE:"+balance);
	}
	public boolean isValidAccount(String accno) {
		if (accno.equals(accNo)) {return true;}
		else {return false;}
	}
//	public void searchAcc(String accno) {
//		
//		if (isValidAccount(accno)) {displayDetails();}
//		//else {System.out.println("INVALID ACCOUNT DETAILS");}
//	}
	public int getBalance() {
		return balance;
	}
	public boolean needToProceed() {
		System.out.println("CHOOSE WHAT TO DO;");
		System.out.println("[1] CONTINUE\n[2] EXIT\n");
		int state = scan.nextInt();
		if(state==1) {return true;}
		else {return false;}
	}
	
}
