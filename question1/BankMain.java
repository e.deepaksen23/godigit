package com.godigit.question1;

import java.util.Scanner;

public class BankMain {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int customersCount=0;
		System.out.println("ENTER THE NUMBER OF CUSTOMERS");
		customersCount= scan.nextInt();
		BankBackEnd customer[]=new BankBackEnd[customersCount];
		for(int i=0; i<customersCount;i++) {
			customer[i]=new BankBackEnd();
			customer[i].createAcc();
		}
		System.out.println("WELCOME USER");
		boolean task=true;
		int flag=0;
		String accno=null;
		do {
			System.out.println("CHOOSE WHAT TO DO;");
			System.out.println("[1] VIEW ALL ACCOUNTS\n[2] SEARCH ACCOUNT\n[3] WITHDRAW AMOUNT\n[4] DEPOSIT AMOUNT\n");
			int action = scan.nextInt();
			
		switch(action) {
		case 1:
			for (int i=0;i<customersCount;i++) {
				customer[i].displayDetails();
				
			}
			task=customer[1].needToProceed();
		    break;
		case 2:
			System.out.println("ENTER ACCOUNT NUMBER");
			accno=scan.next();
			flag =0;
			for (int i=0;i<customersCount;i++) {
				if(customer[i].isValidAccount(accno)) 
				{customer[i].displayDetails();
				flag=1;
				break;}
				}
			if (flag==0) {System.out.println("INVALID ACCOUNT DETAILS");}
			task=customer[1].needToProceed();
			break;
		case 3:
			System.out.println("ENTER ACCOUNT NUMBER");
			accno=scan.next();
			System.out.println("ENTER AMOUNT");
			flag=0;
			int withdrawAmount=scan.nextInt();
			for (int i=0;i<customersCount;i++) {
				if(customer[i].isValidAccount(accno)) {
					customer[i].withdraw(withdrawAmount);
					customer[i].displayDetails();
					flag=1;
					break;}
				}
			if (flag==0){System.out.println("INVALID ACCOUNT DETAILS");}
			task=customer[1].needToProceed();
			break;
		case 4:
			System.out.println("ENTER ACCOUNT NUMBER");
			accno=scan.next();
			System.out.println("ENTER AMOUNT");
			flag=0;
			int depositAmount=scan.nextInt();
			for (int i=0;i<customersCount;i++) {
				if(customer[i].isValidAccount(accno)) {
					customer[i].deposit(depositAmount);
					customer[i].displayDetails();
					flag=1;
					break;}
				
				}
			if (flag==0){System.out.println("INVALID ACCOUNT DETAILS");}
			task=customer[1].needToProceed();
			break;
		}
	}while(task);
	System.out.println("THANKYOU FOR USING ABC BANK");
}
}
