package com.godigit.question3;

import java.util.Scanner;

public class SalaryCalculatorMain {

	public static void main(String[] args) {
		SalaryDetails salary = new SalaryDetails();
		System.out.println("ENTER BASIC PAY");
		Scanner scan = new Scanner(System.in);
		int basicPay=scan.nextInt();
		salary.setBasic(basicPay);
		while(basicPay==0) {
			System.out.println("RE-ENTER BASIC PAY");
			basicPay=scan.nextInt();
			salary.setBasic(basicPay);	
		}
		salary.printTable();
}
}
