package com.godigit.question3;

public class SalaryDetails {
	private int basic;
	private float HRA=0.0F; 
	private float DA=0.0f;
	private float PF=0.0f;
	private float netSalary=0.0f;
	private float grossSalary=0.0f; 
	private float annualPay=0.0f;
	public float getHRA() {
		HRA=basic/10;
		return HRA;
	}
	public float getDA() {
		DA=basic/10;
		return DA;
	}
	public float getPF() {
		PF=basic/10;
		return PF;
	}
	public float getGrossSalary() {
		grossSalary=basic+getDA()+getHRA();
		return grossSalary;
	}
	public float getNetSalary() {
		netSalary=getGrossSalary()-getPF();
		return netSalary;
	}
	public float getAnnualPay() {
		annualPay=getGrossSalary()*12;
		return annualPay;
	}
	public int getBasic() {
		return basic;
	}
	public void setBasic(int base) {
		 boolean isValid=validate(base);
		 if (isValid)
		 {basic=base;
		 System.out.println("BASIC PAY ACCEPTED");		 }
		 else {System.out.println("INVALID BASIC PAY");}
	}
	private boolean validate(int b) {
		if (b>0) {return true;}
		else {return false;}
		
	}
	public void printTable() {
			System.out.println("_________________________________________________");
			System.out.println("|\t\tSALARY DETAILS\t\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| BASIC PAY\t\t | +"+getBasic()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| DEARNESS ALLOWANCE\t | +"+getDA()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| HOUSE RENT ALLOWANCE\t | +"+getDA()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| GROSS PAY\t\t | "+getGrossSalary()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| PROVIDENT FUND\t | -"+getPF()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| NET PAY\t\t | "+getNetSalary()+"\t\t|");
			System.out.println("-------------------------------------------------");
			System.out.println("| ANNUAL PACKAGE\t | "+getAnnualPay()+"\t\t|");
			System.out.println("-------------------------------------------------");
		
		}
	}
